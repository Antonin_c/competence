async function getLog(){ // Création de la function asynchrone pour pouvoir utiliser "await"

   let url = `http://0.0.0.0:8055/auth/login`; // Stocke l'url avec l'adresse IP du sotkcage local dans la var 'url'
   let login = document.getElementById('Login').value;
   let password = document.getElementById('Password').value;
   const data = await fetch (url,{ // contacte L'API
       method: "POST", // Déclaration de la méthode du fetch 
       body:  JSON.stringify({
         "email" : `${login}`,
         "password" : `${password}`
      }),
      headers: {
         "Content-Type":"application/json"
      }
   })
   if(data.ok){ // .ok = si la réponse de l'api est 200
    const response = await data.json(); // la fonction await intérompt l'execution du code pour atendre la réponse 
    console.log(response);
    Token_ID = response['data']['access_token'] // Prend la valeur 'id' du tableau renvoyer par l'api si la réponse est ok
    localStorage.setItem('Token', Token_ID);
    window.location.href="Menu.html";
   }
   else{
       console.log("ERREUR"); // si il n'y a pas de réponse positive 
       logfail()
   }
   }
   function logfail(){
      
   }