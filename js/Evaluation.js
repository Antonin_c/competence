var URL_EVAL = "http://0.0.0.0:8055/items/EVALUATION";
let Eval_Div = document.getElementById("Eval_Card");

async function Get_Eval(){

    while(Eval_Div.firstChild){
        Eval_Div.removeChild(Eval_Div.firstChild);
    }
 
    const data = await fetch (URL_EVAL,{ // contacte L'API
        method:
            "GET", // Déclaration de la méthode du fetch
        headers: {
                "Content-Type":"application/json"
       }
    })

    if(data.ok){ // .ok = si la réponse de l'api est 200
        const response = await data.json();
        //console.log(response);

        response["data"].forEach(async element => {
                 //  console.log(element);
                 var EVALLL_ID = element["ID_EVAL"]
                 let New_Eval = document.createElement("div");
                 New_Eval.setAttribute('id', EVALLL_ID);
                 New_Eval.textContent = element["NOM_EVAL"];
                 New_Eval.setAttribute('class','Evaluation_card')
                 Eval_Div.appendChild(New_Eval);
        
                 let ID_CRIT = element["ID_EVAL"]
        
                 const data2 = await fetch (`http://0.0.0.0:8055/items/DEFINIR?filter[EVALUATION_eval][_eq]=${ID_CRIT}`, { // contacte L'API
                 method:
                     "GET", // Déclaration de la méthode du fetch         
                headers: {
                         "Content-Type":"application/json"
                    }
                })

                    if(data2.ok){ // .ok = si la réponse de l'api est 200
                        let response = await data2.json();
                        //console.log(response);
                        response["data"].forEach(async element =>{

                        let crit_id = element["CRITERE_EVAL_CRIT"];
                            //console.log(crit_id);
                        const data3 = await fetch(`http://0.0.0.0:8055/items/CRITERE?filter[ID_CRIT][_eq]=${crit_id}`, {
                            method:
                                "GET",
                            headers: {
                                    "Content-Type":"application/json"
                            }
                        })
                            if(data3.ok){
                                let response = await data3.json()
                                //console.log(response);
                                response["data"].forEach(async element =>{
                                let ID_DIV = document.getElementById(EVALLL_ID);
                                
                                let New_CRITERE_NOM = document.createElement("div");
                                New_CRITERE_NOM.textContent = element["NOM_CRIT"];

                                let New_CRITERE_NOTE = document.createElement("div");
                                New_CRITERE_NOTE.textContent = element["NOTE_CRIT"]

                                  ID_DIV.appendChild(New_CRITERE_NOM);
                                  ID_DIV.appendChild(New_CRITERE_NOTE);

                                })

                            }else{
                                console.log("ERREUR");
                            }

                    
                    })
                 }
                 else{
                    console.log("ERREUR"); // si il n'y a pas de réponse positive
                 }

        });
    }
    else{
       console.log("ERREUR"); // si il n'y a pas de réponse positive
    }
}