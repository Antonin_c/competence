var URL_BLOC = "http://0.0.0.0:8055/items/BLOC"
let Select_Bloc = document.getElementById("Bloc");
let Select_Domaine = document.getElementById("Domaine");
let Select_Competences = document.getElementById("Selected-Competences");
let count = 1;



async function Bloc(){
 
    const data = await fetch (URL_BLOC,{ // contacte L'API
        method:
            "GET", // Déclaration de la méthode du fetch
        headers: {
                "Content-Type":"application/json"
       }
    })
    if(data.ok){ // .ok = si la réponse de l'api est 200
        const response = await data.json();
        response["data"].forEach( async element => {
            let New_Bloc = document.createElement("option");
            New_Bloc.setAttribute('value', count );
            New_Bloc.textContent = element["NOM_BLOC"];
            count++;

            Select_Bloc.appendChild(New_Bloc);
        });

    }
    else{
       console.log("ERREUR"); // si il n'y a pas de réponse positive
    }
    Domaine(); // appel de la focntion Domaine l'hors du première load
}




async function Domaine(){
    //SUpresion des anciens Resulatat du select

    while(Select_Domaine.firstChild){
        Select_Domaine.removeChild(Select_Domaine.firstChild);
    }
    
    let  Select_Bloc = document.getElementById("Bloc").value;


    const data = await fetch (`http://0.0.0.0:8055/items/DOMAINE?filter[ID_BLOC][_eq]=${Select_Bloc}`, { // contacte L'API
        method:
            "GET", // Déclaration de la méthode du fetch
        headers: {
                "Content-Type":"application/json"
       }
    })
    if(data.ok){ // .ok = si la réponse de l'api est 200
        const response = await data.json();
        let Bloc_Num = document.getElementById("Bloc").value;
        response["data"].forEach( async element => {
            //console.log(element);


            let New_DOMAINE = document.createElement("option");
            New_DOMAINE.setAttribute('value', count);
            New_DOMAINE.setAttribute('id',element["ID_DOMAINE"]);
            New_DOMAINE.textContent = element["NOM_DOMAINE"];
            Select_Domaine.appendChild(New_DOMAINE);
            count++;
        });
    }
    else{
       console.log("ERREUR"); // si il n'y a pas de réponse positive
    }
    Compétence();
}



 async function Compétence(){

    while(Select_Competences.firstChild){
        Select_Competences.removeChild(Select_Competences.firstChild);
    }
     
        let  Select_Comp = document.getElementById("Domaine").options[document.getElementById('Domaine').selectedIndex].id;
     
     //console.log(Select_Comp);

        const data = await fetch (`http://0.0.0.0:8055/items/COMPETENCES?filter[ID_DOMAINE][_eq]=${Select_Comp}`,{ // contacte L'API
             method:
                 "GET", // Déclaration de la méthode du fetch
             headers: {
                 "Content-Type":"application/json"
        }
     })

        if(data.ok){ // .ok = si la réponse de l'api est 200
            const response = await data.json();
            response["data"].forEach( async element => {
            console.log(element);    

            let New_Comp = document.createElement("div");
            New_Comp.setAttribute('value', count);
            New_Comp.setAttribute('id',element["ID_COMPETENCE"]);
            New_Comp.textContent = element["NOM_COMPETENCE"];
            Select_Competences.appendChild(New_Comp);

        });
    }
        else{

            console.log("ERREUR"); // si il n'y a pas de réponse positive
            
        }
 }